Game Kernel Example
=======

About 
-----------
This is a minimal project using GameKernel
[GameKernel](https://gitlab.com/pac85/GameKernel) 

The branch [minimal](https://gitlab.com/pac85/game-kernel-example/-/tree/minimal) contains a barebone project that just
initializes GameKernel

Building
-----------

```
git clone https://gitlab.com/pac85/GameKernel
git clone https://gitlab.com/pac85/egui_vulkano_unsafe egui_vulkano
git clone https://gitlab.com/pac85/imgui-vulkano-renderer
git clone https://github.com/pac85/ruwren
git clone https://github.com/pac85/game-kernel-editor
git clone https://gitlab.com/pac85/game-kernel-example
cd game-kernel-example
cargo run --release 
```

Screenshots
-----------

`TODO`

