mod player;

use std::time::{Instant, Duration};

use game_kernel::{
    ecs::{AddEntity, World},
    utils::{load_gltf, load_gltf_into_world},
};
use player::{add_player_system, setup_input, setup_player};

fn main() {
    let mut sbsts = ::game_kernel::subsystems::init().unwrap();
    sbsts
        .physfs()
        .mount("./files", "/", false)
        .unwrap_or_else(|_| {});

    ::game_kernel::core_systems::init_systems(&mut sbsts);

    //"editor"
    let mut editor_app = game_kernel_editor::App::setup_ui(&mut sbsts);

    sbsts.load("/lights.gkw.json");

    let level_entity = sbsts.world.borrow_mut().add_entity(World::root()).unwrap();
    load_gltf("level.glb", &sbsts, true)
        .load_into_world(&mut *sbsts.world.borrow_mut(), level_entity);

    setup_input(&mut sbsts);
    add_player_system(&mut sbsts);
    setup_player(&mut sbsts);

    let mut should_run = true;
    let mut prev_time = Instant::now();
    // capture mouse or display ui?
    let captured = true;
    while should_run
        && sbsts.process_frame(
            |_delta, _elapsed, sbsts| {
                if !captured {
                    editor_app.ui_frame(sbsts, &mut should_run, true);
                }

                std::thread::sleep(Duration::from_millis(10));
            },
            captured,
            &mut prev_time,
        )
    {}
}
