use cgmath::prelude::*;
use cgmath::*;
use game_kernel::{
    core_systems::{
        physics::{ColliderComponent, PhysicsSystem, RigidBodyComponent, cgna_v3, nacg_v3},
        relative_transform::{RelativeTransformComponent, TransformInheritFlags},
    },
    ecs::*,
    input::Input,
    rapier3d::prelude::*,
    subsystems::Subsystems,
    utils::load_gltf_into_world,
    CameraComponent, CameraSystem, TransformComponent,
};

pub const MAX_ACCELLERATION: f32 = 5.0;
pub const MAX_SPEED: f32 = 10.0;

#[derive(Clone, MetaStruct, Component)]
struct PlayerComponent {
    camera_stick: EntityId,
    camera_angles: Vector2<f32>,
}

impl PlayerComponent {
    fn camera_quat(&self) -> Quaternion<f32> {
        let rotation = Euler::new(
            Deg(0.0),
            Deg(self.camera_angles.x).into(),
            Deg(-self.camera_angles.y).into(),
        );
        Quaternion::from(rotation)
    }
}

fn player_update(manager: &SystemManager, world: &mut World) {
    //calculate delta time
    let mut last_instant = manager.get_resource_mut::<TimeStamp>();
    let delta_time = last_instant.t.elapsed().as_micros() as f32 / 1_000_000.0;
    last_instant.t = std::time::Instant::now();

    let input = manager.get_resource::<Input>();
    let mut a_player_component = None;
    for (_player, rb_component, player_component) in
        world.query::<(EntityId, &mut RigidBodyComponent, &mut PlayerComponent)>()
    {
        if let Some(rb_handle) = rb_component.rigid_body.get_handle() {
            let rb = &mut manager
                .get_system_mut::<PhysicsSystem>()
                .unwrap()
                .rigid_body_set[rb_handle];
            let forward = input.get_control_value("forward").unwrap_or(0.0);
            let side = input.get_control_value("side").unwrap_or(0.0);
            let rot_forward = player_component.camera_quat().rotate_vector(vec3(1.0, 0.0, 0.0));
            // find orthogonal basis that lines up with the up vector
            let up_vec = vec3(0.0, 1.0, 0.0);
            let side_vec = rot_forward.cross(up_vec).normalize();
            let forward_vec = side_vec.cross(up_vec).normalize();
            let up_vec = side_vec.cross(forward_vec);
            let basis_matrix = Matrix3::from_cols(forward_vec, up_vec, side_vec);
            // accelleration is scaled to not go above max speed
            let wish_dir = basis_matrix * vec3(-forward, 0.0, -side);
            if (wish_dir.magnitude() > 0.0001) {
                let wish_vel = wish_dir * MAX_SPEED;
                let push_dir = wish_vel - nacg_v3(rb.linvel());
                let push_len = push_dir.magnitude();
                let push_dir = push_dir.normalize();
                let can_push = push_len.min(MAX_ACCELLERATION * delta_time * MAX_SPEED) / rb.mass();
                let acc = can_push * push_dir * rb.mass();
                rb.apply_impulse(
                    vector![acc.x, 0.0, acc.z],
                    true,
                );
            }

            let mouse = vec2(input.get_control_value("mouse_x").unwrap_or(0.0), input.get_control_value("mouse_y").unwrap_or(0.0));
            player_component.camera_angles = mouse / 10.0;
        }
        a_player_component = Some(player_component.clone());
    }

    if let Some(player_component) = a_player_component {
        let mut transform = world
            .get_entity_component_mut::<RelativeTransformComponent>(player_component.camera_stick)
            .unwrap();
        transform.rotation = player_component.camera_quat();
    }
}

struct TimeStamp {
    t: std::time::Instant,
}

pub fn add_player_system(sbsts: &mut Subsystems) {
    sbsts.systems_manager.add_resource(TimeStamp {
        t: std::time::Instant::now(),
    });
    let mut component_factory = COMPONENT_FACTORY.lock().unwrap();
    component_factory.register::<PlayerComponent>();

    sbsts
        .systems_manager
        .add_and_init(player_update, &mut sbsts.world.borrow_mut())
        .unwrap();
}

pub fn setup_player(sbsts: &mut Subsystems) {
    let player = AddEntity::new(sbsts.world.borrow_mut(), None)
        .unwrap()
        .with_component(TransformComponent::identity())
        .with_component(RigidBodyComponent::from_rigid_body(
            RigidBodyBuilder::dynamic()
                .translation(vector![0.0, 50.0, 0.0])
                .additional_mass(10.0)
                .build(),
        ))
        .with_component(ColliderComponent::from_collider(
            ColliderBuilder::ball(0.5).build(),
        ))
        .entity;

    load_gltf_into_world("meshes/ball.glb", player, sbsts);

    let camera_stick = AddEntity::new(sbsts.world.borrow_mut(), Some(player))
        .unwrap()
        .with_component(
            RelativeTransformComponent::identity()
                .with_inherit_flags(TransformInheritFlags {
                    translation: true,
                    rotation: false, // don't roll the camera and give the player an headache :P
                    scale: false,
                }),
        )
        .entity;

    // add the PlayerComponent with the camera stick id
    sbsts
        .world
        .borrow_mut()
        .add_component(player, PlayerComponent { camera_stick, camera_angles: vec2(0.0, 0.0) });

    let camera_entity = AddEntity::new(sbsts.world.borrow_mut(), Some(camera_stick))
        .unwrap()
        .with_component(RelativeTransformComponent::identity()
                .translate(vec3(-4.0, 2.0, 0.0)))
        .with_component(CameraComponent::new(Default::default()))
        .entity;

    sbsts
        .systems_manager
        .get_system_mut::<CameraSystem>()
        .unwrap()
        .set_active_camera(camera_entity);
}

pub fn setup_input(sbsts: &mut Subsystems) {
    use game_kernel::input::KeyTypes;
    sbsts.input().bind_action(KeyTypes::KeyBoard(17), "forward");
    sbsts
        .input()
        .bind_action(KeyTypes::KeyBoard(31), "backward");
    sbsts.input().bind_control("forward", "forward", 1.0);
    sbsts.input().bind_control("backward", "forward", -1.0);

    sbsts.input().bind_action(KeyTypes::KeyBoard(32), "left");
    sbsts.input().bind_action(KeyTypes::KeyBoard(30), "right");
    sbsts.input().bind_control("left", "side", 1.0);
    sbsts.input().bind_control("right", "side", -1.0);


    sbsts.input().bind_control("mouse_x", "mouse_x", 1.0);
    sbsts.input().bind_control("mouse_y", "mouse_y", 1.0);
}
